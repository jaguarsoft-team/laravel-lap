FROM php:7.3-apache

RUN whoami
RUN apt-get update

WORKDIR /app
COPY . /app

#INSTALA DOCKER
RUN apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
RUN add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
RUN apt-get install -y docker-ce docker-ce-cli containerd.io
RUN apt-get update
RUN docker --version

#INSTALA NODE
#RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
#RUN apt-get update && apt-get install -y nodejs
#RUN apt-get update && apt-get install -y build-essential

#INSTALA PYTHON
#RUN apt-get update && apt-get install -y python-dev
#RUN curl -O https://bootstrap.pypa.io/get-pip.py
#RUN python get-pip.py

#INSTALA AWS CLIENTE
#RUN pip install awscli --upgrade

#PERMITE AL HOST USAR GRADLE CACHE
#VOLUME ["/root/.gradle/caches/"]

#INSTALA GRADLE
#ENV GRADLE_VERSION=2.9
#ENV GRADLE_PATH=/opt/gradle
#RUN wget https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip
#RUN mkdir ${GRADLE_PATH}
#RUN unzip -d ${GRADLE_PATH} gradle-${GRADLE_VERSION}-bin
#ENV GRADLE_HOME=${GRADLE_PATH}/gradle-${GRADLE_VERSION}
#ENV PATH=$PATH:$GRADLE_HOME/bin

#INSTALA GIT
RUN apt-get install -y git-core
RUN git --version

RUN php -v

RUN curl -sS https://getcomposer.org/installer -o composer-setup.php
#RUN curl --silent --show-error https://getcomposer.org/installer | php
RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer
RUN composer
RUN unlink composer-setup.php

#RUN chown -R root /.composer/cache/repo/https---repo.packagist.org/
#RUN chown -R root /.composer/cache/files/